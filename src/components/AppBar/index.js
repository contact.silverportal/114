import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    mainColor: {
        color: "rgba(235, 100, 123, 1)"
    },
    back: {
        height: 12,
        width: 12
    },
    menuButton: {
        // marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        textAlign: "center",
        fontSize: "1.6rem",
        fontWeight: 700,
        textTransform: "uppercase"
    },
}));

export default function ButtonAppBar(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar style={{ background: "white", padding: "10px 0" }} >
                <Toolbar>
                    <IconButton href="./#/7" edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <img
                            className={classes.back}
                            alt="back-btn"
                            src={process.env.PUBLIC_URL + "images/back@2x.png"}
                        />
                    </IconButton>
                    <Typography variant="h6" style={props.font ? { fontSize: "1.5rem" } : {}} className={`${classes.title} ${classes.mainColor}`}>
                        {props.title}
                    </Typography>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <div className={classes.back} ></div>
                    </IconButton>
                </Toolbar>
            </AppBar>
        </div>
    );
}