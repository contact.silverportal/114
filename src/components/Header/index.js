import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
  mainColor: {
    color: "rgba(235, 100, 123, 1)"
  },
  title: {
    fontSize: "1.6rem",
    fontWeight: 700,
    textTransform: "uppercase"
  },
  flex: {
    display: "flex"
  },
  justifyCenter: {
    justifyContent: "center"
  },
  alignCenter: {
    alignItems: "center"
  },
  back: {
    height: 12,
    width: 12
  },
  header: {
    height: "20%",
    width: "100%"
  }
}));

function Header(props) {
  const { title, btm, toLocation } = props;
  const history = useHistory();
  const classes = useStyles();
  const marginBtm = {
    marginBottom: btm,
    position: "relative",
    top: 0,
    left: 0,
    height: "100%",
    width: "100%"
  };

  return (
    <div className={`${classes.header}`}>
      <div
        className={`${classes.flex} ${classes.container} ${classes.justifyCenter} ${classes.alignCenter}`}
        style={marginBtm}
      >
        {/* <Button
          variant="text"
          className={classes.back}
          style={{
            position: "absolute",
            left: -24,
            top: 64,
            padding: "0px !important"
          }}
          onClick={() => {
            history.push(toLocation);
          }}
        >
          <img
            className={classes.back}
            alt="back-btn"
            src={process.env.PUBLIC_URL + "images/back@2x.png"}
          />
        </Button>
        <Typography className={`${classes.title} ${classes.mainColor}`}>
          {title}
        </Typography> */}
        <Button>
          <img
            className={classes.back}
            alt="back-btn"
            src={process.env.PUBLIC_URL + "images/back@2x.png"}
          />
        </Button>
        <Typography className={`${classes.title} ${classes.mainColor}`}>
          {title}
        </Typography>
      </div>
    </div>
  );
}

export { Header };
