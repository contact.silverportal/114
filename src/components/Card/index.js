import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import "./styles.css";

const useStyles = makeStyles(theme => ({
  flex: {
    display: "flex"
  },
  column: {
    flexDirection: "column"
  },
  justifyBetween: {
    justifyContent: "space-between"
  },
  justifyAround: {
    justifyContent: "space-around"
  },
  fontMed: {
    fontSize: 12,
    fontFamily: "Roboto",
    color: "gray"
  },
  bold: {
    fontWeight: 600
  },
  cardWrapper: {
    background: "white",
    borderRadius: 4,
    border: "0.1px ridge #000",
    width: "40%",
    height: 260
    // marginTop: "15%"
  }
}));

export function Card(props) {
  const classes = useStyles();
  const { src, name, dob } = props;

  return (
    <div
      className={`${classes.flex} ${classes.column} ${classes.cardWrapper} shadow`}
    >
      <img
        alt="heart"
        style={{ width: "100%", height: "80%" }}
        src={process.env.PUBLIC_URL + src}
      />
      <div
        style={{ marginLeft: 10, marginTop: 5 }}
        className={`${classes.flex} ${classes.column} ${classes.justifyAround}`}
      >
        <h4 className={`${classes.bold}`}>{name}</h4>
        <div style={{ marginTop: 10 }} className={`${classes.fontMed}`}>
          {dob}
        </div>
      </div>
    </div>
  );
}
