import React, { Fragment } from "react";
import Button from "@material-ui/core/Button";

export function Box(props) {
  const path = props.path;
  const { matches, setMatches } = props;
  function handleOnClick() {
    // if (matches && path === "images/tab da ghep doi normal.png") {
    //   setMatches(!matches);
    // } else if (!matches && path === "images/tab cho ghep doi normal@2x.png") {
    //   setMatches(!matches);
    // }
    if (matches === 1) {
      setMatches(1);
    }
    else if (matches === 2) {
      setMatches(2);
    }
    else if (matches === 3) {
      setMatches(3);
    }

    console.log(props);
  }

  return (
    <Fragment>
      <Button
        variant="text"
        onClick={handleOnClick}
        style={{ width: "53%", height: 104, padding: 5 }}
      >
        <img
          alt="heart"
          style={{ width: "100%", height: "100%" }}
          src={path}
        />
      </Button>
    </Fragment>
  );
}
