import React from "react";
import { Switch, Redirect } from "react-router-dom";

import { RouteWithLayout } from "./components";
import { Main as MainLayout, Minimal as MinimalLayout } from "./layouts";

import {
  LoadPage as LoadView,
  NamePage as NameView,
  DobPage as DobView,
  DesirePage as DesireView,
  FavPage as FavView,
  UploadImagePage as UploadImageView,
  RankPage as RankView,
  VotePage as VoteView,
  FindPage as FindView,
  Page_11 as Page_11View,
  Personal as PersonalView,
} from "./views";

const Routes = () => {
  return (
    <Switch>
      <RouteWithLayout
        component={LoadView}
        exact
        layout={MainLayout}
        path="/"
      />
      <RouteWithLayout
        component={FindView}
        exact
        layout={MinimalLayout}
        path="/7"
      />
      <RouteWithLayout
        component={RankView}
        exact
        layout={MinimalLayout}
        path="/10"
      />
      <RouteWithLayout
        component={Page_11View}
        exact
        layout={MinimalLayout}
        path="/11"
      />
      <RouteWithLayout
        component={VoteView}
        exact
        layout={MinimalLayout}
        path="/13"
      />
      <RouteWithLayout
        component={LoadView}
        exact
        layout={MainLayout}
        path="/dashboard"
      />
      <RouteWithLayout
        component={NameView}
        exact
        layout={MainLayout}
        path="/2"
      />
      <RouteWithLayout
        component={DobView}
        exact
        layout={MainLayout}
        path="/3"
      />
      <RouteWithLayout
        component={PersonalView}
        exact
        layout={MainLayout}
        path="/51"
      />
      <RouteWithLayout
        component={DesireView}
        exact
        layout={MainLayout}
        path="/4"
      />
      <RouteWithLayout
        component={FavView}
        exact
        layout={MainLayout}
        path="/5"
      />
      <RouteWithLayout
        component={UploadImageView}
        exact
        layout={MainLayout}
        path="/6"
      />
      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
