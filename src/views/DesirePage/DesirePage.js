import React, { useState, useEffect } from 'react';
import { makeStyles, withStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';

const ColorButton = withStyles(theme => ({
  root: {
    color: '#000',
    'background-color': '#FFF',
    'font-size': '1.5rem',
    'padding': '20px',
    'padding-left': '20px',
    'padding-top': '10px',
    'min-width': '200px',
    'padding-bottom': '10px',
    'border-radius': '30px',
    backgroundColor: '#FF0',
    '&:hover': {
      backgroundColor: '#F0F',
    },
    '&:active': {
      backgroundColor: '#FF0',
    },
  },
}))(Button);

const useStyles = makeStyles(theme => ({
  root: {
    margin: 'auto',
    display: 'flex',
    'align-items': 'center',
    'justify-content': 'center',
    height: '100%',
    width: '80%'
  },
  img: {
    height: '100%'
  },
  slot: {
    height: '30%',
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  picker__item: {
    background: "white",
    borderRadius: "10px",
    textAlign: "center",
    padding: "6px",
    marginBottom: "10px"
  }
}));

const DesirePage = () => {
  const classes = useStyles();
  const [selectedDate, handleDateChange] = useState(new Date());

  const [allFavorite, setAllFavorite] = useState([]);
  const [pickedFavorite, setPickedFavorite] = useState([]);

  function pickFavorite(index) {
    console.log("pick favorite", index);

    setPickedFavorite([...allFavorite.filter((item, index__item) => {
      return index === index__item;
    }), pickFavorite]);

    setAllFavorite(allFavorite.filter((item, index_item) => {
      return index_item !== index;
    }));

    console.log(allFavorite);
    console.log("this picked", pickFavorite)
  }

  useEffect(() => {

    setAllFavorite([
      "football",
      "basketball",
      "cook",
      "pet"
    ]);

    setPickedFavorite([]);

  }, [])

  return (
    <React.Fragment>
      <div
        className={classes.slot}
      >
        <div className={classes.root}>
          <img
            className={classes.img}
            src="images/page3/title.png"
            alt=""
          />
        </div>
      </div>
      <div
        className={classes.slot}
      >

        <div className="picked__list-favorite" >
          {
            pickedFavorite.map((item, index) => {
              return item;
            })
          }
        </div>
        <div className="picked__hr" >-----------------------</div>
        <div className="picked__all-favorite" >

          {
            allFavorite.map((item, index) => {
              return <div key={index} onClick={() => pickFavorite(index)} className={classes.picker__item} >{item}</div>;
            })
          }
        </div>


      </div>
      <div
        className={classes.slot}
      >
        <div className={classes.root}>
          <ColorButton> Next </ColorButton>
        </div>
      </div>
    </React.Fragment>
  );
};

export default DesirePage;
