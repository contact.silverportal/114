import React, { Fragment, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import { Header, Box } from "components";
import palette from "../../theme/palette";
import AppBar from '../../components/AppBar/index';
import axios from "axios";
import ButtonBase from '@material-ui/core/ButtonBase';
const qs = require("qs");

const useStyles = makeStyles(theme => ({
  root: {
    background: palette.white,
    height: "100%",
    paddingLeft: 12,
    paddingRight: 12,
    paddingTop: 76
  },
  flex: {
    display: "flex"
  },
  column: {
    flexDirection: "column"
  },
  justifyCenter: {
    display: "flex",
    justifyContent: "center"
  },
  justifyBetween: {
    justifyContent: "space-between"
  },
  justifyAround: {
    justifyContent: "space-around"
  },
  justifyEnd: {
    justifyContent: "flex-end"
  },
  alignCenter: {
    alignItems: "center"
  },
  lineContainer: {
    padding: 5,
    height: 32,
    borderRadius: 2
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3)
  },
  choice: {
    paddingBottom: 8
  },
  text: {
    fontSize: 12,
    fontWeight: 500,
    fontFamily: "Roboto"
  },
  mainColor: {
    color: "rgba(235, 100, 123, 1)"
  },
  fontMed: {
    fontSize: 12,
    fontFamily: "Roboto",
    color: "gray",
    textTransform: "capitalize"
  },
  bold: {
    fontWeight: 600
  },
  light: {
    fontWeight: 400
  },
  w60: {
    width: '60%'
  },
  w100: {
    width: '100%'
  }
}));

export default function Page_11_2(props) {
  const classes = useStyles();
  const { matches, setMatches } = props;
  const [token, setToken] = useState(localStorage.getItem("ftptoken"));
  const [requestUser, setRequestUser] = useState([]);
  const [loading,setLoading] = useState(false);
  const [blank,setBlank] = useState(false);

  useEffect(() => {
    setLoading(false);
    async function getData() {
      const options = {
        method: "GET",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          Authorization: token,
          "api-version": "responser"
        },
        url: `https://dps1402.akarpa.io/api/couple?sort=[{"totalLike":"desc"}]&skip=0&limit=100&populate=requestUser&wher`
      };
      try {
        const result = await axios(options);
       
        if (result.data.count > 10) {
          setRequestUser(result.data.data);
          setBlank(false);
        } else {
          setBlank(true);
        }
        setLoading(true);
      } catch (error) {
        if (error) console.log(error);
      }
    }
    getData();

  }, [token]);


  function renderTab(params) {
    return (
      <div
        className={`${classes.flex} ${classes.justifyAround}`}
        style={{ width: "100%", height: "20%" }}
      >
        <Box
          matches={1}
          setMatches={setMatches}
          path="images/page11/dcph_n.png"
        />
        <Box
          matches={2}
          setMatches={setMatches}
          path="images/page11/dcgd_a.png"
        />
        <Box
          matches={3}
          setMatches={setMatches}
          path="images/page11/dgd_n.png"
        />
      </div>
    );
  }

  const backgroundGray = { backgroundColor: "rgba(242, 242, 242, 1)" };
  async function handleCoupleRequest(_id,_status,_itemID) {
    console.log("handle request", _id);

    var _newArr = [];
    for ( var i in requestUser){
      if (requestUser[i].id != _itemID)
      _newArr.push(requestUser[i]);
    }
    setRequestUser(_newArr);
    const status = _status == "Accept" ? 2 : 4
    const options = {
      method: "POST",
      headers: {
        "content-type": "application/x-www-form-urlencoded",
        Authorization: localStorage.getItem("ftptoken"),
        "api-version": "myfpt"
      },
      data: qs.stringify({ status, coupleId: _itemID }),
      url: `https://dps1402.akarpa.io/api/couple/reponse-request`
    };
    try {
      await axios(options);
    } catch (error) {
      if (error) console.log(error);
    }
    
  }

  return (
    <>
      <AppBar title="Người ghép đôi" />
      <div className={classes.root}>
        
        {renderTab()}

        {!loading ? (
        <div class="bubbling-heart">
          <div class="heart"></div>
        </div>
        ) : (
          (!blank) ? (
          <div>
          {requestUser.map((item, ind) => {
            return (
              <div key={ind} style={{ paddingLeft: 12, paddingRight: 12 }}>
                <div
                  className={`${classes.lineContainer} ${classes.flex} ${classes.alignCenter}`}
                  style={ind % 2 === 0  ? backgroundGray : {}}
                >
                  <Avatar
                    alt="Remy Sharp"
                    className={classes.small}
                    src={item.requestUser.avatar}
                  />
                  <p className={`${classes.text} ${classes.mainColor}`}>
                    &nbsp; {item.requestUser.name}
                  </p>
                  <p className={classes.text}>&nbsp; muốn ghép đôi với bạn </p>
                </div>
                <div className={classes.justifyCenter} style={ind % 2 === 0 ? backgroundGray : {}}>
                  <div
                    className={`${classes.flex} ${classes.choice} ${classes.alignCenter}`}
                    style={ind % 2 === 0  ? backgroundGray : {}}
                  >
                    <ButtonBase
                        focusRipple
                        className={`${classes.flex} ${classes.choice} ${classes.alignCenter} ${classes.w60}`}
                        onClick={() => {handleCoupleRequest(item.requestUser.id,"Accept",item.id)}}
                      >
                      <img
                        alt="dongy"
                        src="images/page11/btn_dongy.png"
                        className={classes.w100}
                      />
                    </ButtonBase>
                  </div>
                  <div
                    className={`${classes.flex} ${classes.choice} ${classes.alignCenter}`}
                    style={ind % 2 === 0  ? backgroundGray : {}}
                  >
                    <ButtonBase
                      focusRipple
                      className={`${classes.flex} ${classes.choice} ${classes.alignCenter} ${classes.w60}`}
                      onClick={() => {handleCoupleRequest(item.requestUser.id,"Deny",item.id)}}
                    >
                      <img
                        alt="dongy"
                        src="images/page11/btn_tuchoi.png"
                        className={classes.w100}
                      />
                    </ButtonBase>
                  </div>
                </div>
              </div>
            );
          })}
          </div>
        ) : (<div></div>)
        )
        }
      </div>
      <a className="btnClose" href ="http://myfsoftgame.com/exit">
        <img alt="close" src = "images/btn_x_b.png"></img>
      </a> 
    </>
  );
}