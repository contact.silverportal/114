import React, { useState, Fragment } from "react";
import Page11 from "./Page_11_1";
import Couples from "../12";
import Page12 from './Page_11_2';

export default function Wrapper() {
  const [matches, setMatches] = useState(1);
  return (
    <Fragment>
      {matches === 1 && <Page11 matches={matches} setMatches={setMatches} />}
      {matches === 2 && <Page12 matches={matches} setMatches={setMatches} />}
      {matches === 3 && <Couples matches={matches} setMatches={setMatches} />}
    </Fragment>
  );
}
