import React, { Fragment, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import { Header, Box } from "components";
import palette from "../../theme/palette";
import AppBar from '../../components/AppBar';
import axios from "axios";

const useStyles = makeStyles(theme => ({
  root: {
    background: palette.white,
    height: "100%",
    paddingLeft: 12,
    paddingRight: 12,
    paddingTop: 76
  },
  flex: {
    display: "flex"
  },
  column: {
    flexDirection: "column"
  },
  justifyCenter: {
    justifyContent: "center"
  },
  justifyBetween: {
    justifyContent: "space-between"
  },
  justifyAround: {
    justifyContent: "space-around"
  },
  justifyEnd: {
    justifyContent: "flex-end"
  },
  alignCenter: {
    alignItems: "center"
  },
  lineContainer: {
    padding: 5,
    height: 32,
    borderRadius: 2
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3)
  },
  choice: {
    marginLeft: theme.spacing(3) + 10,
    paddingBottom: 8
  },
  text: {
    fontSize: 12,
    fontWeight: 500,
    fontFamily: "Roboto"
  },
  mainColor: {
    color: "rgba(235, 100, 123, 1)"
  },
  fontMed: {
    fontSize: 12,
    fontFamily: "Roboto",
    color: "gray"
  },
  bold: {
    fontWeight: 600
  },
  light: {
    fontWeight: 400
  },
  listRequest: {
    overflow: "auto",
    height: "79%"
  }
}));

export default function Matches(props) {
  const classes = useStyles();
  const { matches, setMatches } = props;
  const [token, setToken] = useState(localStorage.getItem("ftptoken"));
  const [responseUser, setResponseUser] = useState([]);
  const [loading,setLoading] = useState(false);
  const [blank,setBlank] = useState(false);
  function _preloadIMG(_array,_callback){
    var _count = 0;
    var _newArr = _array;
    const _max = _newArr.length;
    for (var i in _newArr){
      var _img = new Image();
      _img.onload = function(){
        _count += 1;
        if (_count == _max) _callback(_newArr);
      };
      _img.onerror = function(){
        _newArr[this.id].avatar = 'https://scontent.xx.fbcdn.net/v/t1.15752-0/p280x280/66719690_462733461223420_8151055162744504320_n.jpg?_nc_cat=103&_nc_ohc=Nz9vegOrJFcAX-yndWB&_nc_ad=z-m&_nc_cid=0&_nc_zor=9&_nc_ht=scontent.xx&_nc_tp=6&oh=a705b500c0b9bde7c534c03e2899ad0f&oe=5EBEDAD0';
        _count += 1;
        if (_count == _max) _callback(_newArr);
      };
      _img.id = i;
      _img.src = _newArr[i].avatar;
    }
  }

  useEffect(() => {
    setLoading(false);
    async function getData() {
      const options = {
        method: "GET",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          Authorization: token,
          "api-version": "requester"
        },
        url: `https://dps1402.akarpa.io/api/couple?sort=[{"totalLike":"desc"}]&skip=0&limit=100&populate=responseUser`
      };
      try {
        const result = await axios(options);
          setLoading(true);
          if (result.data.data.length < 1){
            setBlank(true);
          } else {
          _preloadIMG(result.data.data,function(data){
            setResponseUser(data);
          });
        }
      } catch (error) {
        if (error) console.log(error);
      }
    }
    getData();

  }, [token]);

  function renderTab(params) {
    return (
      <div
        className={`${classes.flex} ${classes.justifyAround}`}
        style={{ width: "100%", height: "20%" }}
      >
        <Box
          matches={1}
          setMatches={setMatches}
          path="images/page11/dcph_a.png"
        />
        <Box
          matches={2}
          setMatches={setMatches}
          path="images/page11/dcgd_n.png"
        />
        <Box
          matches={3}
          setMatches={setMatches}
          path="images/page11/dgd_n.png"
        />
      </div>
    );
  }

  return (
    <>
      <AppBar title="Người ghép đôi" />
      <div className={classes.root}>
        {renderTab()}
        {!loading ? (
        <div class="bubbling-heart">
          <div class="heart"></div>
        </div>
        ) : ( (!blank) ? (
        <div className={classes.listRequest}>
          {responseUser.map((item, ind) => {
            return (
              <Status
                Name={item.responseUser.name}
                gray={ind % 2 === 0 ? true : false}
              />
            );
          })}
        </div>
        ): (
          <div></div>
        ))
        }
      </div>
      <a className="btnClose" href ="http://myfsoftgame.com/exit">
        <img alt="close" src = "images/btn_x_b.png"></img>
      </a> 
    </>
  );
}

function Status(props) {
  const classes = useStyles();
  const { gray } = props;
  const backgroundGray = { backgroundColor: "rgba(242, 242, 242, 1)" };

  return (
    <div style={{ padding: "0 5px" }}>
      <div
        className={`${classes.lineContainer} ${classes.flex} ${classes.alignCenter}`}
        style={gray ? backgroundGray : {}}
      >

        <p className={classes.text}>&nbsp; Đang chờ &nbsp; &nbsp;</p>
        <Avatar
          alt={props.avatar}
          className={classes.small}
          src={props.avatar}
        />
        <p className={`${classes.text} ${classes.mainColor}`}>
          &nbsp;{props.Name}
        </p>
        <p className={classes.text}>&nbsp; phản hồi</p>
      </div>
    </div >
  );
}



