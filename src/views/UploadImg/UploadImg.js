import React, { useState } from "react";
import { makeStyles, withStyles } from "@material-ui/styles";
import Button from "@material-ui/core/Button";
import axios from "axios";
import { useHistory } from 'react-router-dom';

const ColorButton = withStyles(theme => ({
  root: {
    color: "#000",
    "background-color": "wheat",
    "font-size": "1rem",
    padding: "20px",
    "padding-left": "20px",
    "padding-top": "10px",
    "min-width": "150px",
    "padding-bottom": "10px",
    "border-radius": "30px",
    backgroundColor: "wheat",
    "&:hover": {
      backgroundColor: "wheat"
    },
    "&:active": {
      backgroundColor: "wheat"
    }
  }
}))(Button);

const useStyles = makeStyles(theme => ({
  root: {
    margin: "auto",
    display: "flex",
    "align-items": "center",
    "justify-content": "center",
    height: "100%",
    width: "80%"
  },
  img: {
    width: "100%"
  },
  slot: {
    height: "30%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  w50: {
    width: "50%",
    position: "relative",
    height: "100%",
    overflow: "hidden",
    borderRadius: "15px"
  },
  imgH100: {
    width: "100%"
  },
  inputFile: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    color: "transparent",
    opacity: 0
  }
}));

const UploadImgPage = () => {
  const classes = useStyles();
  let history = useHistory();
  const [loading, setLoading] = useState(true);
  const [imgSrc, setImgSrc] = useState('images/page6/btn.png');

  async function handleChange(e) {
    console.log("handle change called", e.target.files[0]);
    const _token = window.localStorage.getItem("ftptoken");
    console.log(_token);
    try {
      setLoading(false);
      let bodyFormData = new FormData();
      bodyFormData.set("width", 0);
      bodyFormData.set("height", 0);
      bodyFormData.append("images", e.target.files[0]);
      const _urlUploadIMG = "https://dps1402.akarpa.io/api/file/upload-image";
      const _config = {
        headers: {
          "Content-Type": "multipart/form-data",
          "api-version": "common",
          "Authorization": _token
        }
      };
      axios.post(_urlUploadIMG, bodyFormData, _config)
        .then(function (response) {
          console.log(response.data.created[0].url);
          var _user = JSON.parse(window.localStorage.getItem("ftpuser"));
          _user.avatar = response.data.created[0].url;
          window.localStorage.setItem("ftpuser", JSON.stringify(_user));
          setLoading(true);
        })
        .catch(function (error) {
          setLoading(true);
          console.log(error);
        });
    } catch (error) {
      if (error) console.log(error);
    }
  }

  function nextPage() {
    var _user = JSON.parse(window.localStorage.getItem("ftpuser"));
    if (_user.avatar === "") return;
    const _token = window.localStorage.getItem("ftptoken");
    setLoading(false);
    const _config = {
      headers: {
        "Content-Type": "application/json",
        "api-version": "myfpt",
        "Authorization": _token
      }
    };
    var _data = window.localStorage.getItem("ftpuser");
    var _dataObj = JSON.parse(_data);
    const _urlLogin = "https://dps1402.akarpa.io/api/user/" + _dataObj.id;
    axios
    .patch(_urlLogin, _dataObj, _config)
    .then(function (response) {
      console.log(response);
      history.push('./7');
      console.log(response.data.user);
    })
    .catch(function (error) {
      // setTimeout(function () {
      //   //_request();
      // }, 3000);
      console.log(error);
      console.log("can't upload image and manual to ./7");
    });
  }

  return (
    <React.Fragment>
      <div className={classes.slot}>
        <div className={classes.root}>
          <img className={classes.img} src="images/page6/title.png" alt="" />
        </div>
      </div>
      <div className={classes.slot}>
        <div className={classes.w50}>
          <img alt="" src={imgSrc} className={classes.imgH100}></img>
          <input
            className={classes.inputFile}
            onChange={handleChange}
            type="file"
            accept="image/*"
          ></input>
        </div>
      </div>
      <div className={classes.slot}>
        <div className={classes.root}>
          <ColorButton onClick={() => nextPage()} > Tiếp Theo </ColorButton>
        </div>
      </div>
      {!loading ? (
        <div className="fullpage-Loader">
          <div className="wrapLoader">
            <div className="heart"></div>
          </div>
        </div>
      ) : ("")
      }
      <a className="btnClose" href="http://myfsoftgame.com/exit">
        <img alt="close" src="images/btn_x_w.png"></img>
      </a>
    </React.Fragment>
  );
};

export default UploadImgPage;
