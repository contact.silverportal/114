import React from 'react';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%'
  }
}));

const LoadIMG = props => {
  const { url, className } = props;
  const classes = useStyles();
  return (
    <img src = {url} alt = {url} className={className}></img>
  );
};


export default LoadIMG;
