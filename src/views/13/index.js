import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import Swiper from "react-id-swiper";
import "swiper/css/swiper.css";
import { makeStyles } from "@material-ui/core/styles";
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import palette from "../../theme/palette";
import "./style.css";
import axios from "axios";
import Typography from '@material-ui/core/Typography';
import AppBar from '../../components/AppBar/index';
import ButtonBase from '@material-ui/core/ButtonBase';
const qs = require("qs");

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    background: palette.white,
    height: "100%",
    paddingTop: "96px"
  },
  column: {
    flexDirection: "column !important"
  },
  justifyCenter: {
    justifyContent: "center"
  },
  justifyBetween: {
    justifyContent: "space-between"
  },
  justifyAround: {
    justifyContent: "space-around"
  },
  justifyEnd: {
    justifyContent: "flex-end"
  },
  alignCenter: {
    alignItems: "center"
  },
  alignEnd: {
    alignItems: "flex-end"
  },
  bold: {
    fontWeight: 600
  },
  flex: {
    display: "flex !important"
  },
  cardMedia : {
    width: '100%',
    height: '80%'
  },
  cardContent:{
    width: '100%',
    height: '20%'
  },
  cardHearder:{
    padding: "5px",
    height: '20%'
  },
  cardContain:{
    display: 'flex',
    height: '100%',
    width: '45%',
    padding: '5px'
  },
  cards:{
    width: '100%'
  },
  slideText:{
    'white-space': 'nowrap',
    'overflow': 'hidden',
    'text-overflow': 'ellipsis',
    width: '100%',
    'font-size': '2vh'
  }
}));
export default function Vote() {
  const [couples, setCouples] = useState([]);
  const [currentID, setCurrentID] = useState(null);
  const [currentVote, setCurrentVote] = useState(0);
  const [swiper, updateSwiper] = useState(null);

  const [token, setToken] = useState(localStorage.getItem("ftptoken"));
  const classes = useStyles();
  const params = {
    slidesPerView: "auto"
  };
  const [loading,setLoading] = useState(false);
  useEffect(() => {
    setLoading(false);
    async function getData() {
      const skip = "0";
      const limit = "100";
      const options = {
        method: "GET",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          Authorization: token,
          "api-version": "myfpt"
        },
        url: `https://dps1402.akarpa.io/api/couple?sort=[{"totalLike":"desc"}]&skip=${skip}&limit=${limit}&populate=requestUser,responseUser`
      };
      try {
        const result = await axios(options);
        setCouples(result.data.data);
        setLoading(true);
      } catch (error) {
        if (error) console.log(error);
      }
    }
    getData();
  }, [token]);

  useEffect(() => {
    async function getData() {
      const options = {
        method: "GET",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          Authorization: token,
          "api-version": "myfpt"
        },
        url: `https://dps1402.akarpa.io/api/vote?where={"couple":"${currentID}"}`
      };
      try {
        const result = await axios(options);
        if (result.data.count > 0) {
          setCurrentVote(result.data.data[0].voteType);
        }
      } catch (error) {
        if (error) console.log(error);
      }
    }
    getData();
  }, [currentID, token]);

  useEffect(() => {
    if (couples.length > 0) {
      //const active = document.getElementsByClassName("swiper-slide-active")[0].id;
      //setCurrentID(active);
    } else if (couples.length > 0) {
      setCurrentID(couples[0].id);
    }
  }, [couples, couples.length]);

  async function handleChoice(voteType) {
    setCurrentVote(voteType);

    const options = {
      method: "POST",
      headers: {
        "content-type": "application/x-www-form-urlencoded",
        Authorization: token,
        "api-version": "myfpt"
      },
      data: qs.stringify({ voteType, coupleId: currentID }),
      url: `https://dps-1402.herokuapp.com/api/couple/vote`
    };
    try {
      await axios(options);
    } catch (error) {
      if (error) console.log(error);
    }
  }

  function renderChoice() {
    return (
      <div
        style={{ width: "100%", height: "20%", marginTop: "10%" }}
        className={`${classes.flex} ${classes.justifyCenter} ${classes.alignCenter}`}
      >
        <ButtonBase
          focusRipple
          className="btnShadow"
          style={{
            borderRadius: 10,
            background: "white",
            width: "40%",
            margin: "0 10px",
            textTransform: "none",
          }}
          onClick={() => handleChoice(1)}
        >
              <img alt ="tanthanh" src={(currentVote === 1)?"images/page13/btn_tanthanh_active.png":"images/page13/btn_tanthanh.png"} className={classes.imgW}></img>
        </ButtonBase>
        <ButtonBase
          focusRipple
          className="btnShadow"
          variant="contained"
          style={{
            borderRadius: 10,
            background: "white",
            width: "40%",
            margin: "0 15px",
            textTransform: "none",
          }}
          onClick={() => handleChoice(2)}
        >
              <img alt ="phandoi" src={(currentVote === 2)?"images/page13/btn_phandoi_active.png":"images/page13/btn_phandoi.png"} className={classes.imgW}></img>
        </ButtonBase>
      </div>
    );
  }

  return (
    <>
      <AppBar title="bình chọn cặp đôi" font={1.5} />
      <div className={classes.root}>
        {!loading ? (
        <div class="bubbling-heart">
          <div class="heart"></div>
        </div>
        ) : (
        <>
        <div style={{ height: "45%" }}>
          <Swiper getSwiper={updateSwiper} {...params} style={{ height: "100%" }}>
            {couples.map(couple => {
              return (
                <div key={couple.id} className={classes.flex}>
                  <div className={classes.cardContain}>
                  <Card className={classes.cards}>
                  <CardMedia
                    className={classes.cardMedia}
                    image={
                      'https://scontent.xx.fbcdn.net/v/t1.15752-0/p280x280/66719690_462733461223420_8151055162744504320_n.jpg?_nc_cat=103&_nc_ohc=Nz9vegOrJFcAX-yndWB&_nc_ad=z-m&_nc_cid=0&_nc_zor=9&_nc_ht=scontent.xx&_nc_tp=6&oh=a705b500c0b9bde7c534c03e2899ad0f&oe=5EBEDAD0'
                      //couple.requestUser.avatar || "images/avatars/avatar_3.png"
                    }
                    title="..."
                  />
                  <CardHeader
                    title={
                      <Typography className={classes.slideText} variant="h5" component="h5">
                        {couple.requestUser.name}
                      </Typography>
                    }
                    subheader={
                      <Typography className={classes.slideText} variant="h5" component="h5">
                        {couple.requestUser.age}
                      </Typography>
                    }
                    className={classes.cardHearder}
                  />
                  </Card>
                  </div>

                  <div
                    className={`${classes.flex} ${classes.column} ${classes.justifyCenter}`}
                    style={{
                      width: "10%",
                      padding: 5,
                      margin: "0 5px"
                    }}
                  >
                    <img
                      alt="heart"
                      style={{ opacity: 0.8, width: "100%" }}
                      src={process.env.PUBLIC_URL + "images/heart_new.png"}
                    />
                  </div>

                  <div className={classes.cardContain}>
                  <Card className={classes.cards}>
                  <CardMedia
                    className={classes.cardMedia}
                    image={
                      couple.responseUser.avatar || "images/avatars/avatar_3.png"
                    }
                    title="..."
                  />
                  <CardHeader
                    title={
                      <Typography className={classes.slideText} variant="h5" component="h5">
                        {couple.responseUser.name}
                      </Typography>
                    }
                    subheader={
                      <Typography className={classes.slideText} variant="h5" component="h5">
                        {couple.responseUser.age}
                      </Typography>
                    }
                    className={classes.cardHearder}
                  />
                  </Card>
                  </div>
                </div>
              );
            })}
          </Swiper>
        </div>
        {renderChoice()}
        </>
      )}
      </div>
      <a className="btnClose" href ="http://myfsoftgame.com/exit">
        <img alt="close" src = "images/btn_x_b.png"></img>
      </a> 
    </>
  );
}

function Couple(props) {
  const classes = useStyles();
  const { avatar_1, avatar_2, name_1, name_2, dob_1, dob_2 } = props;
  return (
    <div
      className={`${classes.flex}`}
      style={{ width: "100%", height: "100%" }}
    >
      <Card src={avatar_1} name={name_1} dob={dob_1} />
      <div
        className={`${classes.flex} ${classes.column} ${classes.justifyCenter}`}
        style={{
          width: "20%",
          height: 260,
          padding: 10,
          margin: "0 5px"
        }}
      >
        <img
          alt="heart"
          style={{ opacity: 0.8, width: "100%", height: "18%" }}
          src={process.env.PUBLIC_URL + "images/heart_new.png"}
        />
      </div>
      <Card src={avatar_2} name={name_2} dob={dob_2} />
    </div>
  );
}
