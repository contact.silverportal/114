import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import { useHistory } from "react-router-dom";
const qs = require("qs");
const axios = require("axios");

const useStyles = makeStyles(theme => ({
  root: {
    margin: "auto",
    display: "flex",
    "align-items": "center",
    "justify-content": "center",
    height: "100%",
    width: "80%"
  },
  img: {
    width: "100%"
  },
  slot: {
    height: "30%"
  }
}));
async function _request(_callback) {
  const _config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "api-version": "public"
    }
  };
  const _urlLogin = "https://dps1402.akarpa.io/api/auth/sign-in/myfpt";
  const _data = {
    token: window['_token']
  };
  var _user = {
    id: "",
    avatar: "",
    dob: "",
    gender: "",
    phone: "",
    name: "",
    email: "",
    description: "",
    interests: "",
    personalities: "",
    desirablePartner: "",
    isCollectedInfo: 1
  };
  axios
    .post(_urlLogin, qs.stringify(_data), _config)
    .then(function(response) {
      // handle success
      console.log(response);
      console.log("#LOGIN ");
      console.log(response.data.user);

      _user.id = response.data.user.id;
      _user.name = response.data.user.name;
      _user.gender = response.data.user.gender;
      _user.phone = response.data.user.phone;
      _user.email = response.data.user.email;
      _user.description = response.data.user.description;
      _user.interests = response.data.user.interests;
      _user.personalities = response.data.user.personalities;
      _user.desirablePartner = response.data.user.desirablePartner;
      window.localStorage.setItem("ftpuser", JSON.stringify(_user));
      window.localStorage.setItem("ftptoken", response.data.token);
      _callback(response.data.user.isCollectedInfo);
    })
    .catch(function(error) {
      setTimeout(function() {
        _request(_callback);
      }, 5000);
      console.log('FAILED : ');
      console.info('URL : ');
      console.info(window.location.href);
      console.error(JSON.stringify(error.response));
    });
}

const LoadPage = () => {
  const classes = useStyles();
  let history = useHistory();
  useEffect(() => {
    _request(function(_isSkip){
      if (!_isSkip)
      window.location.href = './#/2';
      else
      window.location.href = './#/7';
    });
  }, []);
  
  

  return (
    <React.Fragment>
      <div className={classes.slot}>
        <div className={classes.root}>
          <img className={classes.img} src="images/page1/title.png" alt="load" />
        </div>
      </div>
      <div className={classes.slot}>
        <div className={classes.root}></div>
      </div>
      <div className={classes.slot}>
        <div className={classes.root}></div>
      </div>
      
      <div className="fullpage-Loader">
        <div className="wrapLoader">
         <div className="heart"></div>
        </div>
      </div>
      <div>
      </div>
      <a className="btnClose" href ="http://myfsoftgame.com/exit">
        <img alt="close" src = "images/btn_x_w.png"></img>
      </a>
    </React.Fragment>
  );
};

export default LoadPage;
