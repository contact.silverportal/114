import Page_11 from "./Page_11/Page_11";
export { default as RankPage } from "./10";

export { default as CouplePage } from "./12";
export { default as VotePage } from "./13";
export { default as FavPage } from "./Fav";
export { default as Personal } from "./Personal";
export { default as FindPage } from "./FindPage";
export { default as DesirePage } from "./5";
export { default as UploadImagePage } from "./UploadImg";
export { default as DobPage } from "./page3_dob";
export { default as LoadPage } from "./page1_loadPage";
export { default as NamePage } from "./page2_userinfo";
export { Page_11 };