import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import ButtonBase from '@material-ui/core/ButtonBase';
import "swiper/css/swiper.css";
import Swiper from "react-id-swiper";
import axios from "axios";

const qs = require("qs");


const useStyles = makeStyles(theme => ({
  root: {
    margin: "auto",
    display: "flex",
    "align-items": "center",
    "justify-content": "center",
    height: "100%",
    width: "80%"
  },
  input: {
    width: "100%",
    padding: "10px",
    "border-radius": "30px",
    border: "1px solid",
    "font-size": "1rem",
    outline: "none",
    "padding-left": "20px"
  },
  img: {
    height: "100%"
  },
  slot: {
    height: "100%",
    // border: 'solid black 2px',
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  slot20: {
    height: "25%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  slot40: {
    height: "40%",
    // border: 'solid black 2px',
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  slide: {
    width: "100%",
    height: "50%",
    'padding-top': '10px',
    'padding-bottom': '10px'
  },
  flex: {
    display: 'flex',
    padding: "5px"
  },
  card : {
    height: '100%'
  },
  cardMedia: {
    width: '100%',
    height: '100%'
  },
  cardContent: {
    width: '100%',
    height: '20%'
  },
  cardHearder: {
    padding: '5%',
    height: '20%',
    marginTop: '-40%'
  },
  imgW: {
    width: '100%'
  },
  imgH100: {
    margin:'auto',
    height: "100%",
    position: 'relative',
    transform: 'translateX(-50%)',
    left: '50%'
  },
  footerBtn: {
    width: "25%"
  },
  container: {
    display: 'flex',
    'justify-content': 'space-around',
    width: '100%'
  },
  search: {
    width: "70%"
  },
  slideText: {
    'white-space': 'nowrap',
    'overflow': 'hidden',
    'text-overflow': 'ellipsis',
    width: '100%',
    'font-size': '1.5vh',
    color : '#FFFFFF !important'
  },
  w100: {
    width:'100%'
  }

}));

const UploadImgPage = () => {
  const classes = useStyles();
  const [token, setToken] = useState(localStorage.getItem("ftptoken"));
  var __id = "test";
  if (localStorage.getItem("ftpuser") != null) __id = JSON.parse(localStorage.getItem("ftpuser")).id;
  const [userID, setUserID] = useState(__id);
  const [loading,setLoading] = useState(false);
  const [slideData, setSlideData] = useState([]);
  const [showSlide, setShowSlide] = useState([]);
  const [selectedUser, setSelectedUser] = useState("");

  const params = {
    slidesPerView: 3,
    spaceBetween: 10,
    //loop: true
  };
  
  useEffect(() => {
    setLoading(true);
    async function getData() {
      const options = {
        method: "GET",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          Authorization: token,
          "api-version": "myfpt"
        },
        url: `https://dps1402.akarpa.io/api/couple/get-recomment-user?limit=100&skip=0`
      };
      try {
        const result = await axios(options);
        _preloadIMG(result.data.data,function(data){
          setShowSlide(data);
          setLoading(true);
          window._makeTinderCard();
        });
      } catch (error) {
        if (error) console.log(error);
      }
    }
    getData();
  }, [token]);

  async function searhData(_str) {
    const options = {
      method: "GET",
      headers: {
        "content-type": "application/x-www-form-urlencoded",
        Authorization: token,
        "api-version": "myfpt"
      },
      url: 'https://dps1402.akarpa.io/api/couple/get-recomment-user?limit=100&skip=0&search='+_str
    };
    try {
      const result = await axios(options);
	  setShowSlide(result.data.data);
	  setLoading(true);
      /*_preloadIMG(result.data.data,function(data){
        setShowSlide(data);
      });
	  */
    } catch (error) {
      if (error) console.log(error);
    }
  }

  function SwipeCard(_boolean){
    console.log(_boolean);
    window['createButtonListener'](_boolean);
  }

  window["sendRequest"] = async function(_id,_status) {
    if ((_status === 1) || (_status === 2)) return;    
    const options = {
      method: "POST",
      headers: {
        "content-type": "application/x-www-form-urlencoded",
        Authorization: token,
        "api-version": "myfpt"
      },
      data: qs.stringify({ responser: _id }),
      url: `https://dps1402.akarpa.io/api/couple/request`
    };
    try {
      const result = await axios(options);
      console.log(result);
    } catch (error) {
      if (error) console.log(error);
    }
  };


  function _preloadIMG(_array,_callback){
    var _count = 0;
    var _newArr = _array;
    console.log(_newArr);
    const _max = _newArr.length;
    if (_max === 0) _callback([]);
    for (var i in _newArr){
      var _img = new Image();
      _img.onload = function(){
        _count += 1;
        if (_count == _max) _callback(_newArr);
      };
      _img.onerror = function(){
        _newArr[this.id].avatar = 'https://scontent.xx.fbcdn.net/v/t1.15752-0/p280x280/66719690_462733461223420_8151055162744504320_n.jpg?_nc_cat=103&_nc_ohc=Nz9vegOrJFcAX-yndWB&_nc_ad=z-m&_nc_cid=0&_nc_zor=9&_nc_ht=scontent.xx&_nc_tp=6&oh=a705b500c0b9bde7c534c03e2899ad0f&oe=5EBEDAD0';
        _count += 1;
        if (_count == _max) _callback(_newArr);
      };
      _img.id = i;
      _img.src = _newArr[i].avatar;
    }
  }

  function _status(_num){
    var _return = "";
    if (_num == 0) _return = "Độc thân";
    if (_num == 1) _return = "Đang chờ ghép đôi";
    if (_num == 2) _return = "Đã ghép đôi";
    return _return;
  }

  return (
    <React.Fragment>
      <div
        className={classes.slot}
      >
         {!loading ? (
        <div className="bubbling-heart">
          <div className="heart"></div>
        </div>
        ) : (
          <div className="tinder">
            <div className="tinder--status">
              <div id = "status-nope">
                <img src="images/page7/stt_nope.png" alt="bxh" className={classes.imgH100}></img>
              </div>
              <div id = "status-love">
                <img src="images/page7/stt_love.png" alt="bxh" className={classes.imgH100}></img>
              </div>
            </div>

            <div className="tinder--middle">
              <div>
                <div>
                  <img src="images/page7/nope.png" alt="bxh" className={classes.imgH100}></img>
                </div>
                <div>
                  <img src="images/page7/love.png" alt="bxh" className={classes.imgH100}></img>
                </div>
              </div>
              
            </div>

            <div className="tinder--cards">
              {showSlide.map(data => {
                return (
                    <div className="tinder--card" key={data.id} data-id={data.id} data-status={data.coupleStatus} >
                      <img src= {data.avatar} alt="mm"></img>
                      <Typography className={classes.slideText} variant="h3" component="h3">
                        {data.name || "Vô Danh"}
                      </Typography>
                      <Typography className={classes.slideText} variant="h4" component="h4">
                        {data.age || 'N/A'}
                      </Typography>
                      <Typography className={classes.slideText} variant="h4" component="h4">
                        {_status(data.coupleStatus)}
                      </Typography>
                    </div>
                )
              })}
            </div>
            <div className="tinder--buttons">
              <ButtonBase
                focusRipple
                href="./#/10"
              >
                <img src="images/page7/bxh.png" alt="bxh" className={classes.imgW}></img>
              </ButtonBase>
              <ButtonBase
                focusRipple
                href="./#/13"
              >
                <img src="images/page7/ccd.png" alt="ccd" className={classes.imgW}></img>
              </ButtonBase>
                <ButtonBase
                focusRipple
                href="./#/11"
              >
                <img src="images/page7/ngd.png" alt="ngd" className={classes.imgW}></img>
              </ButtonBase>
            </div>
          </div>
        )
        }
      </div>
      
      <a className="btnClose" href ="http://myfsoftgame.com/exit">
        <img alt="close" src = "images/btn_x_b.png"></img>
      </a>
      
      {!loading ? (
      <div className="bubbling-heart">
        <div className="heart"></div>
      </div>
      ) : ("")
      }
      
    </React.Fragment>
  );
};

export default UploadImgPage;
