import React, { useState, useEffect } from 'react';
import { makeStyles, withStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import axios from "axios";
import { useHistory } from "react-router-dom";

const ColorButton = withStyles(theme => ({
  root: {
    color: '#000',
    'background-color': 'wheat',
    'font-size': '1rem',
    'padding': '20px',
    'padding-left': '20px',
    'padding-top': '10px',
    'min-width': '150px',
    'padding-bottom': '10px',
    'border-radius': '30px',
    backgroundColor: 'wheat',
    '&:hover': {
      backgroundColor: 'wheat',
    },
    '&:active': {
      backgroundColor: 'wheat',
    },
  },
}))(Button);
const AddTagsButton = withStyles(theme => ({
  root: {
    'padding': '10px',
    width: '180px',
    'border-top-left-radius': '30px',
    'border-bottom-left-radius': '30px',
    'border': 'none',
    'font-size': '1.5rem',
    height: "50px",
    '&:hover': {
      backgroundColor: '#F0F',
    },
    '&:active': {
      backgroundColor: '#FF0',
    },
  },
}))(Button);

const useStyles = makeStyles(theme => ({
  root: {
    margin: 'auto',
    display: 'flex',
    'align-items': 'center',
    'justify-content': 'center',
    height: '100%',
    width: '80%'
  },
  img: {
    height: '100%'
  },
  slot30: {
    height: '30%',
    border: 'solid black 2px',
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  slot40: {
    height: '40%',
    border: 'solid black 2px',
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  slot20: {
    height: '20%',
    border: 'solid black 2px',
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  chip: {
    margin: theme.spacing(0.5),
    background: "#FFF"
  },
  break: {
    height: "5px",
    "border-bottom": "1px solid #FFF",
    "margin-bottom": "15px",
    padding: "10px",
    width: "90%"
  },
  pd10: {
    padding: "10px",
  },
  pd10TB: {
    padding: "10px 0",
  },
  input: {
    'padding': '20px',
    width: '50%',
    'border-top-left-radius': '30px',
    'border-bottom-left-radius': '30px',
    'border': 'none',
    'font-size': '1.5rem',
    height: "40px"
  },

  add: {
    width: "20%",
    border: "none",
    height: "40px",
    padding: "10px",
    fontSize: "1rem",
    borderTopRightRadius: "30px",
    borderBottomRightRadius: "30px",
    background: "#ffffff",
    color: "#f0787e",
    borderLeft: "1px solid"
  },
  addTags: {
    display: "flex",
    justifyContent: "center"
  }
}));

function checkExist(item, array) {
  for (let i in array) {
    if (array[i].label === item) {
      return true;
    }
  }
  return false;
}

// Check if max tag bigger 10
function isMaxTag(tagsData) {
  if (tagsData && tagsData.length >= 10) {
    return true;
  }

  return false;
}

const Fav = () => {
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  const [TagsData, setTagsData] = React.useState([]);
  const [existData, setExistData] = React.useState([]);

  useEffect(() => {

    async function getData() {
      const options = {
        method: "GET",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          Authorization: localStorage.getItem("ftptoken"),
          "api-version": "personalities"
        },
        url: `https://dps1402.akarpa.io/api/tag?sort=[{"frequency":"desc"}]&skip=0&limit=100`
      };
      try {
        const result = await axios(options);
        console.log(result);
        if (result.status === 200) {
          setLoading(false);

          if (result.data.data && result.data.data.length > 10) {
            result.data.data.splice(10);
          }

          setExistData(result.data.data.map((item) => {
            return (
              {
                key: item.id,
                label: item.name
              }
            );
          }));
        }
      } catch (error) {
        if (error) console.log(error);
      }
    }
    getData();
  }, []);

  const handleDelete = chipToDelete => () => {
    console.log(chipToDelete);
    console.log([...existData, chipToDelete]);
    if (checkExist(chipToDelete.label, existData) === false) {
      setExistData([...existData, chipToDelete]);
    }
    setTagsData(chips => chips.filter(chip => chip.key !== chipToDelete.key));
  };

  const handleAdd = chipToAdd => () => {
    if (!isMaxTag(TagsData)) {
      setTagsData([...TagsData, chipToAdd]);
      setExistData(chips => chips.filter(chip => chip.key !== chipToAdd.key));
    }
  }

  const [favorite, setFavorite] = React.useState();
  function handleChangeText(e) {
    if (e.target.value && e.target.value.length > 31) {
      e.target.value = e.target.value.substring(0, 31);
    }

    setFavorite(e.target.value);
  }

  function handleAddText() {
    console.log(favorite);

    if (favorite !== undefined && checkExist(favorite, TagsData) === false
      && !isMaxTag(TagsData)) {
      setTagsData([...TagsData, { key: favorite, label: favorite }]);
    }
  }


  let history = useHistory();
  var _user = JSON.parse(window.localStorage.getItem("ftpuser"));
  function nextPage() {
    if (TagsData.length < 1) return;
    var _arrFav = [];
    for (var i in TagsData) {
      _arrFav.push(TagsData[i].label);
    }
    _user.personalities = _arrFav;
    window.localStorage.setItem("ftpuser", JSON.stringify(_user));
    // window.location.href = "/6";
    history.push('./6');
  }

  useEffect(() => {

  }, [])

  return (
    <React.Fragment>
      <div className="cpn__root" >

        <div className=" cpn__single cpn__head">
          <img
            className={classes.img}
            src="images/person/title.png"
            alt=""
          />
        </div>
        <div className="cpn__single cpn__mid-desirePage cpn__max-height-favourite">

          <div className="cpn__mid-tagFavorite">
            {TagsData.map(data => {
              return (
                <Chip
                  key={data.key}
                  label={data.label}
                  onDelete={handleDelete(data)}
                  className={classes.chip}
                />
              );
            })}
          </div>
          <div className={classes.break}></div>
          <div className={classes.pd10TB} >
            {
              loading ? <>Loading</> :
                existData.map(data => {
                  return (
                    <Chip
                      key={data.key}
                      label={data.label}
                      onClick={handleAdd(data)}
                      className={classes.chip}
                    />
                  );
                })
            }
          </div>
          <div className={classes.pd10} >
            <div className={classes.addTags}>
              <input
                className={classes.input}
                onChange={e => handleChangeText(e)}
                type="text"
              />
              <button className={classes.add} onClick={handleAddText} > Thêm </button>
            </div>
          </div>
        </div>
        {/* <div className={classes.breakNoLine}></div> */}
        <div className=" cpn__single cpn__foot" >
          <ColorButton onClick={() => nextPage()}> Tiếp Theo </ColorButton>
        </div>
        <a className="btnClose" href="http://myfsoftgame.com/exit">
          <img alt="close" src="images/btn_x_w.png"></img>
        </a>
      </div>

    </React.Fragment>
  );
};

export default Fav;
