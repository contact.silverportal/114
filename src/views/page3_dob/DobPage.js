import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import "moment/locale/vi";
import "date-fns";
import MomentUtils from "@date-io/moment";
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { useHistory } from "react-router-dom";

const ColorButton = withStyles(theme => ({
  root: {
    color: '#000',
    'background-color': 'wheat',
    'font-size': '1rem',
    'padding': '20px',
    'padding-left': '20px',
    'padding-top': '10px',
    'min-width': '150px',
    'padding-bottom': '10px',
    'border-radius': '30px',
    backgroundColor: 'wheat',
    '&:hover': {
      backgroundColor: 'wheat',
    },
    '&:active': {
      backgroundColor: 'wheat',
    },
  },
}))(Button);

const useStyles = makeStyles(theme => ({
  root: {
    margin: 'auto',
    display: 'flex',
    'align-items': 'center',
    'justify-content': 'center',
    height: '100%',
    width: '80%'
  },
  img: {
    width: '100%'
  },
  slot: {
    height: '30%',
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  input : {
    'padding': '10px',
    'border': 'none',
    'text-align': 'center',
    'border-radius': '60px',
    'font-size': '1.5rem'
  }
}));

const DobPage = () => {
  const classes = useStyles();
  var _user = JSON.parse(window.localStorage.getItem("ftpuser"));
  const [selectedDate, handleDateChange] = useState(new Date());
  let history = useHistory();


  function onChangePickerDate(date) {
    console.log(date._d);
    handleDateChange(date._d);
  }

  function nextPage() {
    var time = new Date(selectedDate);
    _user.dob = time.getFullYear() + "-" + (time.getMonth() > 9 ? time.getMonth() : "0" + time.getMonth()) + "-" + (time.getDate() > 9 ? time.getDate() : "0" + time.getDate());
    console.log("##DOB :", _user.dob);
    window.localStorage.setItem("ftpuser", JSON.stringify(_user));
    history.push('./4');
  }

  return (
    <React.Fragment>
      <div
        className={classes.slot}
      >
        <div className={classes.root}>
          <img
            className={classes.img}
            src="images/page2/title.png"
            alt=""
          />
        </div>
      </div>
      <div
        className={classes.slot}
      >
        <MuiPickersUtilsProvider
          // locale={Locale}
          utils={MomentUtils}
        >
          <DatePicker
            autoOk
            clearable
            disableFuture
            // label="Clearable"
            format={"DD/MM/YYYY"}
            onChange={onChangePickerDate}
            value={selectedDate}
          />
        </MuiPickersUtilsProvider>
      </div>
      {/* </div> */}
      <div
        className={classes.slot}
      >
        <div className={classes.root}>
          <ColorButton onClick={() => nextPage()}> Tiếp Theo </ColorButton>
        </div>
      </div>
      <a className="btnClose" href ="http://myfsoftgame.com/exit">
        <img alt="close" src = "images/btn_x_w.png"></img>
      </a> 
    </React.Fragment>
  );
};

export default DobPage;
