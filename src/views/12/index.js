import React, { Fragment, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Box } from "components";
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import palette from "../../theme/palette";
import "./styles.css";
import AppBar from "../../components/AppBar/index";
import axios from "axios";
import Swiper from "react-id-swiper";
import Typography from '@material-ui/core/Typography';
import "swiper/css/swiper.css";
const qs = require("qs");


const useStyles = makeStyles(theme => ({
  root: {
    background: palette.white,
    height: "100%",
    paddingLeft: 12,
    paddingRight: 12,
    paddingTop: 76
  },
  flex: {
    display: "flex"
  },
  column: {
    flexDirection: "column"
  },
  justifyCenter: {
    justifyContent: "center"
  },
  justifyBetween: {
    justifyContent: "space-between"
  },
  justifyAround: {
    justifyContent: "space-around"
  },
  justifyEnd: {
    justifyContent: "flex-end"
  },
  alignCenter: {
    alignItems: "center"
  },
  alignEnd: {
    alignItems: "flex-end"
  },
  lineContainer: {
    padding: 5,
    height: 32,
    borderRadius: 2
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3)
  },
  text: {
    fontSize: 12,
    fontWeight: 500,
    fontFamily: "Roboto"
  },
  mainColor: {
    color: "rgba(235, 100, 123, 1)"
  },
  fontMed: {
    fontSize: 12,
    fontFamily: "Roboto",
    color: "gray"
  },
  bold: {
    fontWeight: 600
  },
  light: {
    fontWeight: 400
  },
  btnChiatay: {
    width: 108,
    height: 36,
    background: "rgba(251, 226, 144, 1)",
    borderRadius: 18,
    textTransform: "uppercase",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 12,
    fontWeight: 700
  },
  cardMedia: {
    width: '100%',
    height: '80%'
  },
  cardContent: {
    width: '100%',
    height: '20%'
  },
  cardHearder: {
    padding: "5px",
    height: '20%'
  },
  cardContain: {
    display: 'flex',
    height: '100%',
    width: '45%',
    padding: '5px'
  },
  cards: {
    width: '100%'
  },
  slideText: {
    'white-space': 'nowrap',
    'overflow': 'hidden',
    'text-overflow': 'ellipsis',
    width: '100%',
    'font-size': '2vh'
  }
}));

export default function Couples(props) {
  const classes = useStyles();
  const { matches, setMatches } = props;
  const [couples, setCouples] = useState([]);
  const [token, setToken] = useState(localStorage.getItem("ftptoken"));
  const user = JSON.parse(localStorage.getItem("ftpuser"));
  const [loading,setLoading] = useState(false);
  const [blank,setBlank] = useState(false);
  const [shareID,setShareID] = useState("");
  const params = {
    slidesPerView: 1,
    spaceBetween: 10
  };
  var _imageData = "";
  useEffect(() => {
    setLoading(false);
    async function getData() {
      const options = {
        method: "GET",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          Authorization: token,
          "api-version": "myfpt"
        },
        url: `https://dps1402.akarpa.io/api/couple?sort=[{"totalLike":"desc"}]&skip=0&limit=100&where={"or":[{"requestUser":"${user.id}"},{"responseUser":"${user.id}"}]}&populate=requestUser,responseUser`
      };
      try {
        const result = await axios(options);
        setLoading(true);
        if (result.data.data.length < 1) {
          setBlank(true);
        } else {
          setShareID(result.data.data[0].id);
          setCouples(result.data.data);
        }
      } catch (error) {
        if (error) console.log(error);
      }
    }
    getData();

  }, [token, user.id]);

  async function handleBreak(id) {
    console.log("## CHIA TAY " + id);
    const options = {
      method: "POST",
      headers: {
        "content-type": "application/x-www-form-urlencoded",
        Authorization: localStorage.getItem("ftptoken"),
        "api-version": "myfpt"
      },
      data: qs.stringify({ coupleId: id }),
      url: `https://dps1402.akarpa.io/api/couple/reponse-request`
    };
    try {
      await axios(options);
    } catch (error) {
      if (error) console.log(error);
    }

  }

  async function _sharedBtn(_str){
    setLoading(false);
    const optionShare = {
      method: "GET",
      headers: {
        "content-type": "application/x-www-form-urlencoded",
        Authorization: token,
        "api-version": "myfpt"
      },
      url: 'https://dps1402.akarpa.io/api/couple/get-share-image?coupleId='+shareID
    };
    const imageShare = await axios(optionShare);
    window.location.href = 'http://myfpt.com.share'+_str+'?image='+imageShare.data.message+'&message=CONTENTSHARE'
  }

  function renderTab() {
    return (
      <div
        className={`${classes.flex} ${classes.justifyAround}`}
        style={{ width: "100%", height: "20%" }}
      >
        <Box
          matches={1}
          setMatches={setMatches}
          path="images/page11/dcph_n.png"
        />
        <Box
          matches={2}
          setMatches={setMatches}
          path="images/page11/dcgd_n.png"
        />
        <Box
          matches={3}
          setMatches={setMatches}
          path="images/page11/dgd_a.png"
        />
      </div>
    );
  }


  return (
    <>
      <AppBar title="Người ghép đôi" />
      <div className={classes.root}>
        {/* <Header title="Người ghép đôi" btm={32} /> */}
        {renderTab()}
        {!loading ? (
        <div class="bubbling-heart">
          <div class="heart"></div>
        </div>
        ) : (  (!blank) ? (
        <>
        <div
          style={{
            height: '50%'
          }}
        >
          <div
            className={`${classes.flex}`}
            style={{ width: "100%", height: "100%", marginTop: 10 }}
          >
            <div style={{ width: "100%", height: "70%" }}>
              <Swiper {...params} style={{ width: "100%", height: "100%" }} shouldSwiperUpdate>
                {couples.map(couple => {
                  return (
                    <div key={couple.id} className={classes.flex}>
                      <div className={classes.cardContain}>
                        <Card className={classes.cards}>
                          <CardMedia
                            className={classes.cardMedia}
                            image={
                              couple.requestUser.avatar || "images/avatars/avatar_3.png"
                            }
                            title="..."
                          />
                          <CardHeader
                            title={
                              <Typography className={classes.slideText} variant="h5" component="h5">
                                {couple.requestUser.name}
                              </Typography>
                            }
                            subheader={
                              <Typography className={classes.slideText} variant="h5" component="h5">
                                {couple.requestUser.age}
                              </Typography>
                            }
                            className={classes.cardHearder}
                          />
                        </Card>
                      </div>

                      <div
                        className={`${classes.flex} ${classes.column} ${classes.justifyCenter}`}
                        style={{
                          width: "10%",
                          padding: 5,
                          margin: "0 5px"
                        }}
                      >
                        <img
                          alt="heart"
                          style={{ opacity: 0.8, width: "100%" }}
                          src={process.env.PUBLIC_URL + "images/heart_new.png"}
                        />
                      </div>

                      <div className={classes.cardContain}>
                        <Card className={classes.cards}>
                          <CardMedia
                            className={classes.cardMedia}
                            image={
                              couple.responseUser.avatar || "images/avatars/avatar_3.png"
                            }
                            title="..."
                          />
                          <CardHeader
                            title={
                              <Typography className={classes.slideText} variant="h5" component="h5">
                                {couple.responseUser.name}
                              </Typography>
                            }
                            subheader={
                              <Typography className={classes.slideText} variant="h5" component="h5">
                                {couple.responseUser.age}
                              </Typography>
                            }
                            className={classes.cardHearder}
                          />
                        </Card>
                      </div>
                    </div>
                  );
                })}
              </Swiper>
            </div>
          </div>
        </div>
        <div style={{ margin: "10px 0 10px 0" }}
            className={`${classes.flex} ${classes.justifyCenter}`}
          >
            <Button variant="text"
              onClick={() => handleBreak("1234")}
            >
              <img
                style={{ width: 90, height: 28 }}
                alt="face-share"
                src={process.env.PUBLIC_URL + "images/bt chiatay - normal.png"}
              />
            </Button>
          </div>
          <div className={`${classes.flex} ${classes.justifyAround}`}>
            <Button variant="text"
              onClick={() => {_sharedBtn("FB")}}
            >
              <img
                alt="face-share"
                style={{ width: 90, height: 28 }}
                src={process.env.PUBLIC_URL + "images/face share@2x.png"}
              />
            </Button>

            <Button variant="text"
              onClick={() => {_sharedBtn("WP")}}
            >
              <img
                alt="chat"
                style={{ width: 90, height: 28 }}
                src={process.env.PUBLIC_URL + "images/workchat@2x.png"}
              />{" "}
            </Button>
          </div>
      </>
        ): (
          <div></div>)
        )}
      </div>
      <a className="btnClose" href ="http://myfsoftgame.com/exit">
        <img alt="close" src = "images/btn_x_b.png"></img>
      </a> 
    </>
  );
}
