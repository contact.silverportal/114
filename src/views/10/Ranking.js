import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import palette from "../../theme/palette";
import axios from "axios";
import AppBar from '../../components/AppBar/index';

const useStyles = makeStyles(theme => ({
  root: {
    background: palette.white,
    height: "100%",
    paddingTop: 76,
    paddingLeft: 12,
    paddingRight: 12
  },
  flex: {
    display: "flex"
  },
  justifyCenter: {
    justifyContent: "center"
  },
  justifyBetween: {
    justifyContent: "space-between"
  },
  justifyAround: {
    justifyContent: "space-around"
  },
  alignCenter: {
    alignItems: "center"
  },
  mainColor: {
    color: "rgba(235, 100, 123, 1)"
  },
  greyColor: {
    color: "rgba(129, 129, 129, 1)"
  },
  lineContainer: {
    padding: 5,
    height: 42
  },
  lineCouple: {},
  medal: {
    height: 18,
    width: 18
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3)
  },
  text: { fontSize: 14, fontWeight: 600 }
}));

export default function Ranking() {
  const [couples, setCouples] = useState([]);
  const [token, setToken] = useState(localStorage.getItem("ftptoken"));
  const classes = useStyles();
  const [loading,setLoading] = useState(false);
  
  useEffect(() => {
    setLoading(false);
    async function getData() {
      const skip = "0";
      const limit = "100";
      const options = {
        method: "GET",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          Authorization: token,
          "api-version": "myfpt"
        },
        url: `https://dps1402.akarpa.io/api/couple?sort=[{"totalLike":"desc"}]&skip=${skip}&limit=${limit}&populate=requestUser,responseUser`
      };
      try {
        const result = await axios(options);
        setCouples(result.data.data);
        setLoading(true);
      } catch (error) {
        if (error) console.log(error);
      }
    }
    getData();
  }, [token]);

  return (
    <>
      <AppBar title="BẢNG XẾP HẠNG" />
      <div className={classes.root}>
        {!loading ? (
        <div class="bubbling-heart">
          <div class="heart"></div>
        </div>
        ) : (
        <>
        {couples.map((couple, ind) => {
          const gray = ind % 2 === 0 ? true : false;
          let medal;
          switch (ind) {
            case 0:
              medal = "images/page10/1.png";
              break;
            case 1:
              medal = "images/page10/2.png";
              break;
            case 2:
              medal = "images/page10/3.png";
              break;
            default:
              medal = null;
              break;
          }
          return (
            <Line
              key={couple.id}
              firstAvatar={
                couple.requestUser.avatar
              }
              secondAvatar={
                couple.responseUser.avatar
              }
              like={couple.totalLike}
              dislike={couple.totalDisLike}
              medal={medal}
              gray={gray}
            />
          );
        })}
        </>
        )}
      </div>
      <a className="btnClose" href ="http://myfsoftgame.com/exit">
        <img alt="close" src = "images/btn_x_b.png"></img>
      </a> 
    </>
  );
}

function Line(props) {
  const classes = useStyles();
  const { firstAvatar, secondAvatar, like, dislike, gray } = props;
  const backgroundGray = { backgroundColor: "rgba(242, 242, 242, 1)" };
  function renderCouple() {
    return (
      <div
        className={`${classes.flex} ${classes.justifyAround} ${classes.alignCenter}`}
        style={{ width: "25%" }}
      >
        <Avatar alt="Remy Sharp" className={classes.small} src={firstAvatar} />
        <img
          alt="heart"
          style={{ height: 11 }}
          src={process.env.PUBLIC_URL + "images/heart.png"}
        />
        <Avatar alt="Remy Sharp" className={classes.small} src={secondAvatar} />
      </div>
    );
  }
  function renderReaction({ type, number, content }) {
    const path = type === "like" ? "images/page10/like.png" : "images/page10/unlike.png";
    const width = type === "like" ? "32%" : "28%";
    const imgHeight = type === "like" ? 12 : 10;
    return (
      <div
        className={`${classes.flex} ${classes.justifyBetween} ${classes.alignCenter}`}
        style={{ height: "100%", width }}
      >
        <img
          alt="like"
          style={{ height: imgHeight }}
          src={process.env.PUBLIC_URL + path}
        />
        <p
          className={`${
            type === "like" ? classes.mainColor : classes.greyColor
            } ${classes.text}`}
        >
          {number}
        </p>
        <p className={classes.text}>{content}</p>
      </div>
    );
  }

  return (
    <div
      className={`${classes.lineContainer} ${classes.flex} ${classes.alignCenter} ${classes.justifyAround}`}
      style={gray ? backgroundGray : {}}
    >
      <div
        className={`${classes.flex} ${classes.alignCenter}`}
        style={{ height: "100%", width: "5%" }}
      >
        {props.medal ? (
          <img
            alt="medal-gold"
            className={classes.medal}
            src={process.env.PUBLIC_URL + props.medal}
          />
        ) : null}
      </div>
      {renderCouple()}
      {renderReaction({
        type: "like",
        number: like,
        content: "Tán thành"
      })}
      {renderReaction({
        type: "dislike",
        number: dislike,
        content: "Phản đối"
      })}
    </div>
  );
}
