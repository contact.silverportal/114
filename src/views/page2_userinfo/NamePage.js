import React, { useState, useEffect } from 'react';
import { makeStyles, withStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import { useHistory } from 'react-router-dom';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

const ColorButton = withStyles(theme => ({
  root: {
    color: '#000',
    'background-color': 'wheat',
    'font-size': '1rem',
    'padding': '20px',
    'padding-left': '20px',
    'padding-top': '10px',
    'min-width': '150px',
    'padding-bottom': '10px',
    'border-radius': '30px',
    backgroundColor: 'wheat',
    '&:hover': {
      backgroundColor: 'wheat',
    },
    '&:active': {
      backgroundColor: 'wheat',
    },
  },
}))(Button);

const useStyles = makeStyles(theme => ({
  root: {
    margin: 'auto',
    display: 'flex',
    'align-items': 'center',
    'justify-content': 'center',
    height: '100%',
    width: '80%'
  },
  img: {
    width: '80%'
  },
  slot: {
    height: '30%'
  },
  input: {
    'width': '100%',
    'padding': '10px',
    'border-radius': '30px',
    'border': 'none',
    'font-size': '1.5rem',
    'outline': 'none',
    "padding-left": "20px",
    "text-align": "center"
  },
  textArea: {
    width: "80%",
    borderRadius: "15px",
    padding: "10px",
    outline: "none",
    fontSize: "1rem",
    height: "100%",
    border: "none"
  }
}));

const NamePage = () => {
  const classes = useStyles();
  var _user = JSON.parse(window.localStorage.getItem("ftpuser"));
  let history = useHistory();
  //console.log(_user.name);
  const [name, setName] = useState("");

  useEffect(() => {

  }, []);

  function handleChangeName(e) {
    setName(e.target.value)
  }
  function setScroll(){
    window.scrollTo(0, 0)
  }
  function nextPage() {
    if (name === "") return;
    _user.description = name;
    window.localStorage.setItem("ftpuser", JSON.stringify(_user));
    history.push("./3");
  }

  return (
    <React.Fragment>
      <div className="cpn__root" >

        <div className=" cpn__single cpn__head">
          <img
            className={classes.img}
            src="images/page2/title.png"
            alt=""
          />
        </div>
        <div className=" cpn__single cpn__mid" >
          <TextareaAutosize
            className={classes.textArea} style={{ height: "100%" }} rows={10} aria-label="minimum height" rowsmin={3}
            value={name}
            onChange={handleChangeName}
            onBlur={setScroll}
            placeholder="Đôi điều về bản thân.."
          >
          </TextareaAutosize>
        </div>
        <div className=" cpn__single cpn__foot" >
          <ColorButton onClick={() => nextPage()} > Tiếp Theo </ColorButton>
        </div>

      </div>
      <a className="btnClose" href ="http://myfsoftgame.com/exit">
        <img alt="close" src = "images/btn_x_w.png"></img>
      </a> 
    </React.Fragment >
  );
};

export default NamePage;
