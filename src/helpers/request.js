/*
import socketIOClient from 'socket.io-client';
import sailsIOClient from 'sails.io.js';


// Instantiate the socket client (`io`)
// (for now, you must explicitly pass in the socket.io client when using this library from Node.js)
var io = sailsIOClient(socketIOClient);

io.sails.url = Config.host;
io.sails.autoConnect = true;
io.socket.on('connect', () => {
    console.log('connect success');

})

io.socket.on('disconnect', function () {
    console.log('disconnect');
});

io.socket.on("reconnecting", (numAttempts) => {
    console.log('reconnecting: numAttempts=>', numAttempts);
})
io.socket.on("reconnect", (transport, numAttempts) => {
    console.log('reconnect: numAttempts=>', numAttempts, { transport });
})
*/
var Config = {
    host: 'https://dps1402.akarpa.io',
    debug: true
}
let request = {};

request.callRest = async (url, data, headers, method = 'POST') => {
    url = `${Config.host}${url}`;
    let option = {
        method, // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
            'api-version': 'myfpt',
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': `${localStorage.getItem('ftptoken') || 'customer'}`
        }
    };
    option.headers = Object.assign({}, option.headers, headers);
    if (method === 'GET') delete option.body;

    if (Config.debug) console.log(`[${method}]`, url, option);

    let res = await fetch(url, option);
    try {
        let rs = await res.json();
        if (Config.debug) console.log(`[RESPONSE]`, url, rs);
        switch (res.status) {
            case 401:
                let location = window.location;
                //Local.clear();
                sessionStorage.setItem('unauthorized', 1)
                if (!(+sessionStorage.getItem('aftersignin'))) {
                    sessionStorage.setItem('lastLocation', location);
                }
                window.location.href = '/';
                throw rs;
            case 200:
                return rs;
            default:
                throw rs;
        }

    } catch (err) {
        console.log('res', res, err);
        throw err;
    }

}

/*
request.ioRequest = async (url, data, headers, method = 'POST') => {
    url = `${Config.host}${url}`;
    let option = {
        url,
        method, // or 'PUT'
        data,
        headers: {
            'api-version': 'pageid',
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': `${localStorage.getItem('ftptoken') || 'customer'}`
        }
    };
    option.headers = Object.assign({}, option.headers, headers);
    // if (method === 'GET') delete option.data;

    if (Config.debug) console.log(`[${method}]`, url, option);
    let res = await new Promise((resolve, reject) => {
        io.socket.request(option, function (resData, jwres) {
            resolve(jwres)
        });
    })

    try {
        let rs = await res.body;
        if (Config.debug) console.log(`[RESPONSE]`, url, rs);
        switch (res.statusCode) {
            case 401:
                let location = window.location;
                Local.clear();
                sessionStorage.setItem('unauthorized', 1)
                if (!(+sessionStorage.getItem('aftersignin'))) {
                    sessionStorage.setItem('lastLocation', location);
                }
                window.location.href = '/';
                throw rs;
            case 200:
                return rs;
            default:
                throw rs;
        }

    } catch (err) {
        console.log('res', res, err);
        throw err;
    }

}
request.socket = io.socket;
*/
export default request;